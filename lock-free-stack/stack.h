#pragma once

#include <stack>
#include <mutex>

template <class T>
class Stack {
public:
    void Push(const T& value) {
        std::lock_guard<std::mutex> guard(lock_);
        data_.push(value);
    }

    bool Pop(T* value) {
        std::lock_guard<std::mutex> guard(lock_);
        if (data_.empty()) {
            return false;
        }
        *value = std::move(data_.top());
        data_.pop();
        return true;
    }

    size_t Size() const {
        std::lock_guard<std::mutex> guard(lock_);
        return data_.size();
    }

    void Clear() {
        while (!data_.empty()) {
            data_.pop();
        }
    }

private:
    std::stack<T> data_;
    mutable std::mutex lock_;
};
